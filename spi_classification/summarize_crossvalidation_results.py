import numpy as np
from copy import deepcopy
from batchgenerators.utilities.file_and_folder_operations import *

from spi_classification.paths import results_folder

if __name__ == '__main__':
    #models = [i for i in subdirs(results_folder, join=False) if all([isfile(join(results_folder, i, 'fold_%d' % f, 'results.json')) for f in range(5)])]
    models = [i for i in [s for s in subdirs(results_folder, join=False)] if
              all([isfile(join(results_folder, i, 'fold_%d' % f, 'results.json')) for f in range(5)])]

    with open(join(results_folder, 'summary_allcv.csv'), 'w') as gf:
        global_metrics = None
        for m in models:
            results = []
            for f in range(5):
                results.append(load_json(join(results_folder, m, 'fold_%d' % f, 'results.json')))

            metrics = list(results[0].keys())
            if global_metrics is None:
                global_metrics = deepcopy(metrics)
                gf.write("model")
                for metric in global_metrics:
                    gf.write(",%s" % metric)
                gf.write("\n")

            results_by_metric = {
                metric:list() for metric in metrics
            }

            with open(join(results_folder, m, 'summary_cv.csv'), 'w') as f:
                f.write('fold')
                for metric in metrics:
                    f.write(",%s" % metric)
                f.write("\n")
                for fl in range(5):
                    f.write("fold_%d" % fl)
                    for metric in metrics:
                        f.write(",%0.4f" % results[fl][metric])
                        results_by_metric[metric].append(results[fl][metric])
                    f.write("\n")

            gf.write(m)
            for metric in global_metrics:
                gf.write(",%s" % np.mean(results_by_metric[metric]))
            gf.write("\n")
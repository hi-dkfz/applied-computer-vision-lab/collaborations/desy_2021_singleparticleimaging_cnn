import numpy as np
import argparse

import spi_classification
from batchgenerators.utilities.file_and_folder_operations import *

from spi_classification.network_training.preact_resnet import PaperConfigMaxF1
from spi_classification.paths import results_folder
from nnunet.training.model_restore import recursive_find_python_class


if __name__ == "__main__":
    a = argparse.ArgumentParser()
    a.add_argument('trainerClass', help='name of the trainer class')
    a.add_argument('-f', nargs='+', type=str, required=False, default=(0, 1, 2, 3, 4),
                   help='folds used for prediction. Default: 0 1 2 3 4 (all folds)')
    a.add_argument('-o', help='oputput file (truncated. .json/.csv will be added. Default: None (= no output file '
                              'is generated).', required=False, default=None)
    a.add_argument('--tta', action='store_true', help='set this flag to enable test time augmentation (mirroring). '
                                                      'Reduces inference speed by ~factor 4')
    a.add_argument('-m', type=str, default='model_final_checkpoint.model', required=False,
                   help='model checkpoint filename. Default: model_final_checkpoint.model')

    args = a.parse_args()
    trainer = args.trainerClass

    trainer_class = recursive_find_python_class([join(spi_classification.__path__[0])], trainer, "spi_classification")
    assert issubclass(trainer_class, PaperConfigMaxF1)

    t = trainer_class(args.f[0])
    t.initialize(training=False)

    t.load_dataset()
    predictions = []
    folds = [int(i) if i != 'all' else i for i in args.f]

    checkpoints = [join(results_folder, t.__class__.__name__, 'fold_%s' % str(f), args.m) for f in folds]

    for c in checkpoints:
        t.load_checkpoint(c, train=False)
        predictions.append(t.predict_images(t.test_files, tta=args.tta)[0])

    gt = t.test_gt
    predictions_mean = np.array(predictions).mean(0)
    assert predictions_mean.shape[0] == len(gt)
    metrics = t.compute_metrics(predictions_mean, gt)
    print(metrics)

    if args.o is not None:
        # export predictions
        export = np.hstack((
            np.array([os.path.basename(i) for i in t.test_files])[:, None],
            np.round(predictions_mean[:, 1:], 4),
            (predictions_mean[:, 1:] > 0.5).astype(int),
            np.array(t.test_gt)[:, None]
        ))
        export = np.vstack((np.array(['image_name', 'probability_single_hit', 'prediction_single_hit', 'ground_truth']), export))
        np.savetxt(args.o + ".csv", export, fmt='%s', delimiter=',')
        save_json(metrics, args.o + ".json")

import torch
from batchgenerators.utilities.file_and_folder_operations import join
from dynamic_network_architectures.building_blocks.plain_conv_encoder import PlainConvEncoder

from nnunet.network_architecture.generic_modular_UNet import PlainConvUNetEncoder, get_default_network_config
from torch import nn
from torch.nn import AdaptiveAvgPool2d

from spi_classification.network_training.preact_resnet import PaperConfigMaxF1


class MaxF1VGG(
    PaperConfigMaxF1):
    def initialize_network(self):
        model = PlainConvUNetEncoder(
            1,
            base_num_features=16,
            num_blocks_per_stage=(2, 2, 2, 3),
            feat_map_mul_on_downscale=2,
            pool_op_kernel_sizes=((1, 1), (2, 2), (2, 2), (2, 2)),
            conv_kernel_sizes=((3, 3), (3, 3), (3, 3), (3, 3)),
            props=get_default_network_config(nonlin='ReLU'),
            default_return_skips=False,
            max_num_features=256,
        )
        output_channels = model.output_features
        model = nn.Sequential(model, AdaptiveAvgPool2d(1), nn.Conv2d(output_channels, 2, 1, 1, 0, 1, bias=True)).cuda()
        self.network = model

        try:
            import hiddenlayer as hl
            g = hl.build_graph(model, torch.rand((self.batch_size, 1, *self.patch_size)).cuda(), transforms=None)
            g.save(join(self.output_folder, 'network_architecture.pdf'))
        except ImportError:
            pass


class MaxF1VGGdynV2NormReLU(PaperConfigMaxF1):
    def initialize_network(self):
        model = PlainConvEncoder(
            1, 4, (16, 32, 64, 128), nn.Conv2d, ((3, 3), (3, 3), (3, 3), (3, 3)), ((1, 1), (2, 2), (2, 2), (2, 2)),
            2, True, nn.BatchNorm2d, None, None, None, nn.ReLU, {'inplace': True}, False, False
        )
        output_channels = 128
        model = nn.Sequential(model, AdaptiveAvgPool2d(1), nn.Conv2d(output_channels, 2, 1, 1, 0, 1, bias=True)).cuda()
        self.network = model

        try:
            import hiddenlayer as hl
            g = hl.build_graph(model, torch.rand((self.batch_size, 1, *self.patch_size)).cuda(), transforms=None)
            g.save(join(self.output_folder, 'network_architecture.pdf'))
        except ImportError:
            pass


class MaxF1VGGdynV2ReLUNorm(PaperConfigMaxF1):
    def initialize_network(self):
        model = PlainConvEncoder(
            1, 4, (16, 32, 64, 128), nn.Conv2d, ((3, 3), (3, 3), (3, 3), (3, 3)), ((1, 1), (2, 2), (2, 2), (2, 2)),
            2, True, nn.BatchNorm2d, None, None, None, nn.ReLU, {'inplace': True}, False, True
        )
        output_channels = 128
        model = nn.Sequential(model, AdaptiveAvgPool2d(1), nn.Conv2d(output_channels, 2, 1, 1, 0, 1, bias=True)).cuda()
        self.network = model

        try:
            import hiddenlayer as hl
            g = hl.build_graph(model, torch.rand((self.batch_size, 1, *self.patch_size)).cuda(), transforms=None)
            g.save(join(self.output_folder, 'network_architecture.pdf'))
        except ImportError:
            pass


class MaxF1VGGdynV2ReLUNormAlexFeat(PaperConfigMaxF1):
    def initialize_network(self):
        model = PlainConvEncoder(
            1, 4, (16, 32, 128, 256), nn.Conv2d, ((3, 3), (3, 3), (3, 3), (3, 3)), ((1, 1), (2, 2), (2, 2), (2, 2)),
            2, True, nn.BatchNorm2d, None, None, None, nn.ReLU, {'inplace': True}, False, True
        )
        output_channels = 256
        model = nn.Sequential(model, AdaptiveAvgPool2d(1), nn.Conv2d(output_channels, 2, 1, 1, 0, 1, bias=True)).cuda()
        self.network = model

        try:
            import hiddenlayer as hl
            g = hl.build_graph(model, torch.rand((self.batch_size, 1, *self.patch_size)).cuda(), transforms=None)
            g.save(join(self.output_folder, 'network_architecture.pdf'))
        except ImportError:
            pass


class MaxF1VGGdynV2ReLUNormAlexFeatMaxPool(PaperConfigMaxF1):
    def initialize_network(self):
        model = PlainConvEncoder(
            1, 4, (16, 32, 128, 256), nn.Conv2d, ((3, 3), (3, 3), (3, 3), (3, 3)), ((1, 1), (2, 2), (2, 2), (2, 2)),
            2, True, nn.BatchNorm2d, None, None, None, nn.ReLU, {'inplace': True}, False, True, 'max'
        )
        output_channels = 256
        model = nn.Sequential(model, AdaptiveAvgPool2d(1), nn.Conv2d(output_channels, 2, 1, 1, 0, 1, bias=True)).cuda()
        self.network = model

        try:
            import hiddenlayer as hl
            g = hl.build_graph(model, torch.rand((self.batch_size, 1, *self.patch_size)).cuda(), transforms=None)
            g.save(join(self.output_folder, 'network_architecture.pdf'))
        except ImportError:
            pass


class MaxF1VGGdynV2ReLUNormFabianMaxPool(
    PaperConfigMaxF1):
    def initialize_network(self):
        model = PlainConvEncoder(
            1, 6, (16, 32, 64, 128, 256, 256), nn.Conv2d, ((3, 3), (3, 3), (3, 3), (3, 3), (3, 3), (3, 3)), ((1, 1), (2, 2), (2, 2), (2, 2), (2, 2), (2, 1)),
            2, True, nn.BatchNorm2d, None, None, None, nn.ReLU, {'inplace': True}, False, True, 'max'
        )
        output_channels = 256
        model = nn.Sequential(model, AdaptiveAvgPool2d(1), nn.Conv2d(output_channels, 2, 1, 1, 0, 1, bias=True)).cuda()
        self.network = model

        try:
            import hiddenlayer as hl
            g = hl.build_graph(model, torch.rand((self.batch_size, 1, *self.patch_size)).cuda(), transforms=None)
            g.save(join(self.output_folder, 'network_architecture.pdf'))
        except ImportError:
            pass


class MaxF1VGGFabian(
    PaperConfigMaxF1):
    def initialize_network(self):
        model = PlainConvUNetEncoder(
            1,
            base_num_features=16,
            num_blocks_per_stage=(2, 2, 2, 3, 3, 3),
            feat_map_mul_on_downscale=2,
            pool_op_kernel_sizes=((1, 1), (2, 2), (2, 2), (2, 2), (2, 2), (2, 1)),
            conv_kernel_sizes=((3, 3), (3, 3), (3, 3), (3, 3), (3, 3), (3, 3)),
            props=get_default_network_config(nonlin='ReLU'),
            default_return_skips=False,
            max_num_features=256,
        )
        output_channels = model.output_features
        model = nn.Sequential(model, AdaptiveAvgPool2d(1), nn.Conv2d(output_channels, 2, 1, 1, 0, 1, bias=True)).cuda()
        self.network = model

        try:
            import hiddenlayer as hl
            g = hl.build_graph(model, torch.rand((self.batch_size, 1, *self.patch_size)).cuda(), transforms=None)
            g.save(join(self.output_folder, 'network_architecture.pdf'))
        except ImportError:
            pass


class MaxF1VGGAlexandr(
    PaperConfigMaxF1):
    def initialize_network(self):
        model = VGGAlexandr().cuda()
        self.network = model

        try:
            import hiddenlayer as hl
            g = hl.build_graph(model, torch.rand((self.batch_size, 1, *self.patch_size)).cuda(), transforms=None)
            g.save(join(self.output_folder, 'network_architecture.pdf'))
        except ImportError:
            pass


class VGGAlexandr(nn.Module):
    def __init__(self):
        super().__init__()
        self.block1 = nn.Sequential(
            nn.Conv2d(1, 16, 3, 1, 1),
            nn.ReLU(inplace=True),
            nn.BatchNorm2d(16),
            nn.Conv2d(16, 16, 3, 1, 1),
            nn.ReLU(inplace=True),
            nn.BatchNorm2d(16),
            nn.MaxPool2d(2, 2)
        )
        self.block2 = nn.Sequential(
            nn.Conv2d(16, 32, 3, 1, 1),
            nn.ReLU(inplace=True),
            nn.BatchNorm2d(32),
            nn.Conv2d(32, 32, 3, 1, 1),
            nn.ReLU(inplace=True),
            nn.BatchNorm2d(32),
            nn.MaxPool2d(2, 2)
        )
        self.block3 = nn.Sequential(
            nn.Conv2d(32, 128, 3, 1, 1),
            nn.ReLU(inplace=True),
            nn.BatchNorm2d(128),
            nn.Conv2d(128, 128, 3, 1, 1),
            nn.ReLU(inplace=True),
            nn.BatchNorm2d(128),
            nn.MaxPool2d(2, 2)
        )
        self.block4 = nn.Sequential(
            nn.Conv2d(128, 256, 3, 1, 1),
            nn.ReLU(inplace=True),
            nn.BatchNorm2d(256),
            nn.Conv2d(256, 256, 3, 1, 1),
            nn.ReLU(inplace=True),
            nn.BatchNorm2d(256),
            nn.Conv2d(256, 256, 3, 1, 1),
            nn.ReLU(inplace=True),
            nn.BatchNorm2d(256),
            nn.MaxPool2d(2, 2) # not useful
        )
        self.gap = nn.AdaptiveAvgPool2d(1)
        self.output = nn.Conv2d(256, 2, 1, 1, 0, 1, bias=True)

    def forward(self, x):
        return self.output(self.gap(self.block4(self.block3(self.block2(self.block1(x))))))

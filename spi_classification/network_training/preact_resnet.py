from multiprocessing import Pool
from time import time

import numpy as np
import torch
from batchgenerators.dataloading.multi_threaded_augmenter import MultiThreadedAugmenter
from batchgenerators.transforms.abstract_transforms import Compose
from batchgenerators.transforms.color_transforms import NormalizeTransform, GammaTransform
from batchgenerators.transforms.crop_and_pad_transforms import RandomShiftTransform
from batchgenerators.transforms.noise_transforms import BlankRectangleTransform, GaussianNoiseTransform, \
    GaussianBlurTransform
from batchgenerators.transforms.spatial_transforms import SpatialTransform_2, MirrorTransform
from batchgenerators.transforms.utility_transforms import NumpyToTensor
from batchgenerators.utilities.file_and_folder_operations import *
from batchgenerators.utilities.file_and_folder_operations import load_pickle
from nnunet.network_architecture.generic_modular_UNet import get_default_network_config
from nnunet.network_architecture.generic_modular_preact_residual_UNet import PreActResidualUNetEncoder
from nnunet.training.learning_rate.poly_lr import poly_lr
from nnunet.training.network_training.network_trainer import NetworkTrainer
from nnunet.utilities.set_n_proc_DA import get_allowed_n_proc_DA
from nnunet.utilities.to_torch import to_cuda, maybe_to_torch
from torch import nn
from torch.cuda.amp import autocast
from torch.nn import AdaptiveAvgPool2d
from torch.optim import Adam

from spi_classification.data_preparation_and_management.dataloader import SPIDataLoader
from spi_classification.data_preparation_and_management.png_export import export_png
from spi_classification.paths import results_folder, base, image_source_folder


class PaperConfigMaxF1(NetworkTrainer):
    def __init__(self, fold):
        super().__init__(False, True)

        self.output_folder = join(results_folder, self.__class__.__name__, 'fold_%s' % str(fold))
        maybe_mkdir_p(self.output_folder)
        self.fold = int(fold)
        self.train_files = self.train_gt = self.val_files = self.val_gt = None
        self.tr_transforms = self.val_transforms = None
        self.loss = nn.CrossEntropyLoss()

        self.test_files = None
        self.test_gt = None
        allowed_n_proc = get_allowed_n_proc_DA()
        self.num_processes_DA = allowed_n_proc if allowed_n_proc is not None else 12
        self.patch_size = (192, 96)
        self.batch_size = 64
        self.max_num_epochs = 1000
        self.num_batches_per_epoch = 50
        self.num_val_batches_per_epoch = 10
        self.save_every = 100

        self.initial_lr = 1e-4

        self.tp = []
        self.fp = []
        self.fn = []
        self.accs = []

        self.patience = 999999999

        self.plot_every = 10

        self.oversample = 0.02

    def plot_progress(self):
        if (self.epoch + 1) % 10 == 0:
            super().plot_progress()

    def initialize_optimizer_and_scheduler(self):
        self.optimizer = Adam(self.network.parameters(), self.initial_lr, amsgrad=True)

    def maybe_update_lr(self, epoch=None):
        if epoch is None:
            ep = self.epoch + 1
        else:
            ep = epoch
        self.optimizer.param_groups[0]['lr'] = poly_lr(ep, self.max_num_epochs, self.initial_lr, 0.9)
        self.print_to_log_file("lr was set to:", np.round(self.optimizer.param_groups[0]['lr'], decimals=6))

    def on_epoch_end(self) -> bool:
        ret = super().on_epoch_end()
        if any(np.isnan(self.all_tr_losses)):
            raise RuntimeError('BOOOM')
        return ret

    def load_dataset(self):
        splits = load_pickle(join(base, 'final_200k_split_191183.pkl'))

        self.train_gt = splits['cross_validation'][self.fold]['train_gt']
        train_identifiers = splits['cross_validation'][self.fold]['train_identifiers']
        self.train_files = [join(image_source_folder, i + '.npz') for i in train_identifiers]

        self.val_gt = splits['cross_validation'][self.fold]['val_gt']
        val_identifiers = splits['cross_validation'][self.fold]['val_identifiers']
        self.val_files = [join(image_source_folder, i + '.npz') for i in val_identifiers]

        self.test_gt = splits['test_set']['test_gt']
        test_identifiers = splits['test_set']['test_identifiers']
        self.test_files = [join(image_source_folder, i + '.npz') for i in test_identifiers]

        print('training set:', sum(self.train_gt), 'single hits', len(self.train_files), 'total')
        print('validation set:', sum(self.val_gt), 'single hits', len(self.val_files), 'total')
        print('test set:', sum(self.test_gt), 'single hits', len(self.test_files), 'total')

    def get_da_pipeline(self):
        """
        Changelog
        - mirror both axes
        - more random shift because that seemed to help (5->10)
        - higher p for Spatial and shift
        - add rotation
        - add blank squares
        :return:
        """
        mn, std = 0.34182968016243553, 2.336387769378462
        tr_transforms = [
            NormalizeTransform([mn], [std]),
            SpatialTransform_2(patch_size=self.patch_size, patch_center_dist_from_border=999999, do_elastic_deform=True,
                               do_rotation=True, do_scale=True, angle_x=(- 90 / 180 * np.pi, 90 / 180 * np.pi),
                               scale=(0.75, 1.25), random_crop=False, p_el_per_sample=0.3, p_scale_per_sample=0.3,
                               p_rot_per_sample=0.3,
                               independent_scale_for_each_axis=True),
            GammaTransform((0.75, 1.5)),
            GaussianNoiseTransform((0, 0.05), p_per_sample=0.1),
            GaussianBlurTransform((0, 0.5), p_per_sample=0.15),
            MirrorTransform((0, 1)),
            RandomShiftTransform(0, 10, 0.75, 1),
            BlankRectangleTransform(((1, 80), (1, 40)), (-mn) / std, (1, 6.1), False, p_per_channel=1,
                                                  p_per_sample=1),
            NumpyToTensor(['data', 'target'])
        ]
        val_tr = [NormalizeTransform([mn], [std]), NumpyToTensor(['data', 'target'])]
        return Compose(tr_transforms), Compose(val_tr)

    def initialize_network(self):
        cfg = get_default_network_config()
        cfg['conv_op_kwargs']['bias'] = False
        model = PreActResidualUNetEncoder(
            1,
            base_num_features=16,
            num_blocks_per_stage=(1, 1, 1, 1, 2, 2),
            feat_map_mul_on_downscale=2,
            pool_op_kernel_sizes=((1, 1), (2, 2), (2, 2), (2, 2), (2, 2), (2, 1)),
            conv_kernel_sizes=((3, 3), (3, 3), (3, 3), (3, 3), (3, 3), (3, 3)),
            props=cfg,
            default_return_skips=False,
            max_num_features=256
        )
        output_channels = model.output_features
        model = nn.Sequential(model, AdaptiveAvgPool2d(1), nn.Conv2d(output_channels, 2, 1, 1, 0, 1, bias=True)).cuda()
        self.network = model
        try:
            import hiddenlayer as hl
            g = hl.build_graph(model, torch.rand((self.batch_size, 1, *self.patch_size)).cuda(), transforms=None)
            g.save(join(self.output_folder, 'network_architecture.pdf'))
        except:
            pass

    def initialize(self, training=True):
        self.tr_transforms, self.val_transforms = self.get_da_pipeline()
        if training:
            self.load_dataset()

            dl_tr = SPIDataLoader((self.train_files, self.train_gt), self.batch_size, self.num_processes_DA, None, True, True, True, oversample=self.oversample)
            dl_val = SPIDataLoader((self.val_files, self.val_gt), self.batch_size, self.num_processes_DA//2, None, True, True, True, oversample=self.oversample)

            self.tr_gen = MultiThreadedAugmenter(dl_tr, self.tr_transforms, self.num_processes_DA, 2, None, True)
            self.val_gen = MultiThreadedAugmenter(dl_val, self.val_transforms, self.num_processes_DA//2, 2, None, True)

        self.initialize_network()
        self.initialize_optimizer_and_scheduler()

        self.was_initialized = True

    def predict_images(self, list_of_filenames, batch_size=None, tta=False):
        status = self.network.training
        self.network.eval()

        if batch_size is None:
            batch_size = 4 * self.batch_size

        data_loader = SPIDataLoader((list_of_filenames, [0] * len(list_of_filenames)), batch_size, 8, 1, True, False, False)
        mt_data_loader = MultiThreadedAugmenter(data_loader, self.val_transforms, 8, 2, None, True)

        st = time()
        fnames = []
        predictions = []
        with torch.no_grad():
            for b in mt_data_loader:
                d_cuda = to_cuda(maybe_to_torch(b['data']))
                output = self.network(d_cuda).squeeze()
                if tta:
                    output += self.network(torch.flip(d_cuda, (2, ))).squeeze()
                    output += self.network(torch.flip(d_cuda, (3, ))).squeeze()
                    output += self.network(torch.flip(d_cuda, (2, 3))).squeeze()
                    output /= 4
                output = torch.softmax(output, 1).detach().cpu().numpy()
                predictions.append(output)
                fnames += b['fnames']
        end = time()
        self.print_to_log_file("prediction took %0.4f s" % (end - st), '. That\'s %0.4f images/s' % (len(fnames) / (end-st)))

        # safety check. There is one duplicate training example. Someone (not me) did not pay attention
        assert abs(len(np.unique(fnames)) - len(fnames)) <= 1
        self.network.train(status)
        mt_data_loader._finish()
        return np.vstack((predictions)), fnames

    @staticmethod
    def compute_metrics(y_pred, y_true):
        """
        :param y_pred: (num_images x 2) np.ndarray
        :param y_true: (num_images) list or np.ndarray
        :return:
        """
        assert len(y_pred) == len(y_true)

        hard_predictions = y_pred.argmax(1)
        y_true = np.array(y_true) if isinstance(y_true, (tuple, list)) else y_true
        acc = np.sum(hard_predictions == y_true) / len(y_true)

        tp = np.sum(hard_predictions[y_true == 1])
        fp = np.sum(hard_predictions[y_true == 0])
        fn = np.sum(y_true[hard_predictions == 0])
        f1 = 2 * tp / (2 * tp + fp + fn)

        precision = tp / (tp + fp)
        recall = tp / (tp + fn)

        all_metrics = {'acc': acc,
                       'f1': f1,
                       'precision': precision,
                       'recall': recall,
                       'total_cases': int(len(y_true)),
                       'tp': float(tp),
                       'fp': float(fp),
                       'fn': float(fn),
                       'num_1_gt': int(sum(y_true)),
                       'num_1_pred': int(sum(hard_predictions)),
                       'iou': float(tp / (tp+fp+fn))
                       }
        return all_metrics

    def export_pngs(self, fnames, y_predicted, y_true, num_processes=None):
        if num_processes is None:
            num_processes = self.num_processes_DA

        png_dir = join(self.output_folder, os.path.pardir, 'pngs')
        tp_dir = join(png_dir, 'true_positives')
        fn_dir = join(png_dir, 'false_negatives')
        fp_dir = join(png_dir, 'false_positives')
        tn_dir = join(png_dir, 'true_negatives')
        maybe_mkdir_p(tp_dir)
        maybe_mkdir_p(fn_dir)
        maybe_mkdir_p(fp_dir)
        maybe_mkdir_p(tn_dir)

        p = Pool(num_processes)
        res = []
        for im, gt, pred in zip(fnames, y_true, y_predicted):
            pred_hard = pred.argmax()
            if gt == 1 and pred_hard == 1:
                exp_fname = join(tp_dir, os.path.basename(im)[:-4] + '.png')
            elif gt == 0 and pred_hard == 1:
                exp_fname = join(fp_dir, os.path.basename(im)[:-4] + '.png')
            elif gt == 1 and pred_hard == 0:
                exp_fname = join(fn_dir, os.path.basename(im)[:-4] + '.png')
            else:
                exp_fname = join(tn_dir, os.path.basename(im)[:-4] + '.png')
            res.append(p.starmap_async(export_png, ((np.load(im)['data'], exp_fname, pred_hard, gt, pred[1],
                                                     self.fold, 'jet', None), )))
            #export_png(np.load(im)['data'], exp_fname, pred, gt, p_1, 'jet', None)
        _ = [i.get() for i in res]
        p.close()
        p.join()

    def validate(self, export_pngs=False):
        if self.val_files is None:
            self.load_dataset()

        predictions, fnames = self.predict_images(self.val_files, self.batch_size)
        with open(join(self.output_folder, 'valset_predictions.csv'), 'w') as f:
            f.write("image_id,fold,gt,pred,prob_class_1\n")
            for im, gt, pred_soft in zip(fnames, self.val_gt, predictions):
                f.write("%s,%s,%d,%d,%0.4f\n" % (os.path.basename(im)[:-4], str(self.fold), gt, pred_soft.argmax(), pred_soft[1]))

        if export_pngs:
            self.export_pngs(self.val_files, predictions, self.val_gt)

        self.print_to_log_file("got", len(predictions), "validation cases")

        all_metrics = self.compute_metrics(predictions, self.val_gt)
        save_json(all_metrics, join(self.output_folder, "results.json"))
        self.print_to_log_file(all_metrics)

    def test(self, export_pngs=False):
        if self.test_files is None:
            self.load_dataset()

        predictions, fnames = self.predict_images(self.test_files)

        with open(join(self.output_folder, 'testset_predictions.csv'), 'w') as f:
            f.write("image_id,fold,gt,pred,prob_class_1\n")
            for im, gt, pred_soft in zip(fnames, self.test_gt, predictions):
                f.write("%s,%s,%d,%d,%0.4f\n" % (os.path.basename(im)[:-4], str(self.fold), gt, pred_soft.argmax(), pred_soft[1]))

        if export_pngs:
            self.export_pngs(self.test_files, predictions, self.test_gt)

        self.print_to_log_file("got", len(predictions), "test cases")

        all_metrics = self.compute_metrics(predictions, self.test_gt)
        save_json(all_metrics, join(self.output_folder, "results_test.json"))
        self.print_to_log_file(all_metrics)

    def run_online_evaluation(self, output, target):
        output_cpu = output.argmax(1).squeeze().detach().cpu().numpy()
        target_cpu = target.detach().cpu().numpy()
        acc = np.sum(output_cpu == target_cpu) / len(target)
        self.tp.append(np.sum(output_cpu[target_cpu == 1]))
        self.fp.append(np.sum(output_cpu[target_cpu == 0]))
        self.fn.append(np.sum(target_cpu[output_cpu == 0]))
        self.accs.append(acc)

    def finish_online_evaluation(self):
        self.print_to_log_file("Accuracy:", np.mean(self.accs))
        self.accs = []

        tp = np.sum(self.tp)
        fp = np.sum(self.fp)
        fn = np.sum(self.fn)

        self.tp, self.fp, self.fn = [], [], []

        dc = 2 * tp / (2 * tp + fp + fn)
        self.all_val_eval_metrics.append(dc)
        self.print_to_log_file("F1 score", dc)

    def run_iteration(self, data_generator, do_backprop=True, run_online_evaluation=False):
        data_dict = next(data_generator)
        data = data_dict['data']
        target = data_dict['target']

        data = maybe_to_torch(data)
        target = maybe_to_torch(target.long())

        if torch.cuda.is_available():
            data = to_cuda(data)
            target = to_cuda(target)

        self.optimizer.zero_grad()

        if self.fp16:
            with autocast():
                output = self.network(data)
                del data
                l = self.loss(output.squeeze(), target)

            if do_backprop:
                self.amp_grad_scaler.scale(l).backward()
                self.amp_grad_scaler.step(self.optimizer)
                self.amp_grad_scaler.update()
        else:
            output = self.network(data)
            del data
            l = self.loss(output, target)

            if do_backprop:
                l.backward()
                self.optimizer.step()

        if run_online_evaluation:
            self.run_online_evaluation(output, target)

        del target

        return l.detach().cpu().numpy()


class PaperConfigMoreSingleHits(PaperConfigMaxF1):
    def __init__(self, fold):
        super().__init__(fold)
        self.loss = nn.CrossEntropyLoss(weight=torch.Tensor((0.1, 0.9)).to(0))
        self.oversample = 0.05

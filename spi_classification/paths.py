from batchgenerators.utilities.file_and_folder_operations import *


base = os.environ.get('DESY_BASE')
results_folder = join(base, 'results')
image_source_folder = join(base, '200k_dataset')

if results_folder is not None:
    maybe_mkdir_p(results_folder)

import argparse

import spi_classification
from batchgenerators.utilities.file_and_folder_operations import *
from nnunet.training.model_restore import recursive_find_python_class


if __name__ == "__main__":
    a = argparse.ArgumentParser()
    a.add_argument('trainerClass')
    a.add_argument('fold')
    a.add_argument("-val", "--validation_only", help="use this if you want to only run the validation",
                    action="store_true")
    a.add_argument("-c", "--continue_training", help="",
                    action="store_true")
    a.add_argument("--export_pngs", help="",
                    action="store_true")
    a.add_argument("--disable_saving", help="",
                    action="store_true")
    a.add_argument("--find_lr", help="",
                   action="store_true")
    a.add_argument("--pred_test", help="",
                   action="store_true")
    args = a.parse_args()

    trainer = args.trainerClass
    valonly = args.validation_only

    trainer_class = recursive_find_python_class([join(spi_classification.__path__[0])], trainer, "spi_classification")

    t = trainer_class(args.fold)
    t.initialize(not valonly)

    if args.disable_saving:
        t.save_final_checkpoint = False # whether or not to save the final checkpoint
        t.save_best_checkpoint = False  # whether or not to save the best checkpoint according to
        # self.best_val_eval_criterion_MA
        t.save_intermediate_checkpoints = True  # whether or not to save checkpoint_latest. We need that in case
        # the training crashes
        t.save_latest_only = True  # if false it will not store/overwrite _latest but separate files each

    if args.continue_training:
        t.load_latest_checkpoint(True)

    if args.find_lr:
        t.find_lr()
    else:
        if not valonly:
            t.run_training()
        else:
            t.load_final_checkpoint(False)
            pass

        t.validate(export_pngs=args.export_pngs)

    if args.pred_test:
        t.load_final_checkpoint(False)
        t.test(export_pngs=args.export_pngs)

import h5py
import numpy as np
from batchgenerators.utilities.file_and_folder_operations import subfiles, maybe_mkdir_p, join
from spi_classification.data_preparation_and_management.dataset import crop_and_save
from multiprocessing import Pool

from spi_classification.paths import image_source_folder


def export_file(input_file, output_folder):
    num_samples = 0
    run_according_to_file = input_file.split("/")[-1].split("run_")[-1][:-3]
    with h5py.File(input_file, 'r') as f:
        entries = list(f.keys())
        print(input_file)
        for entry in entries:
            # print(input_file, entry, list(f[entry].keys()))
            indices = np.array(f[entry]['image_1']['index'])
            if len(np.unique(indices)) != len(indices):
                indices = ['fakeindex_%d' % i for i in np.arange(len(indices))]
            data = f[entry]['image_1']['data']
            for d, r, i in zip(data, [run_according_to_file] * len(indices), indices):
                try:
                    crop_and_save(d, join(output_folder, '%s__%s.npz' % (r, i)))
                    num_samples += len(indices)
                except IOError as e:
                    print("Error processing:", input_file, entry, r, i)
                    raise e
    return num_samples


def main():
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('input_folder', type=str, help='folder with the preprocessed data. This folder should contain 82 h5 files')
    parser.add_argument('-num_processes', type=int, default=8, help='number of processes (CPU cores) used for patch extraction. Default: 8')
    args = parser.parse_args()
    input_folder = args.input_folder
    maybe_mkdir_p(image_source_folder)

    h5_files = subfiles(input_folder, suffix='h5')
    assert len(h5_files) == 82, f'Expected 82 h5 files. Got {len(h5_files)}.'
    r = []
    p = Pool(args.num_processes)
    for filename in h5_files:
        r.append(p.starmap_async(export_file, ((filename, image_source_folder), )))
    _ = [i.get() for i in r]
    p.close()
    p.join()


if __name__ == '__main__':
    # base = '/home/isensee/Downloads/data_corrected'
    # output_folder = image_source_folder
    main()

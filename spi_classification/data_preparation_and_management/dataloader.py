import numpy as np
from batchgenerators.dataloading.data_loader import DataLoader


class SPIDataLoader(DataLoader):
    def __init__(self, data, batch_size, num_threads_in_multithreaded=1, seed_for_shuffle=None, return_incomplete=False,
                 shuffle=True, infinite=False, image_size=(192, 96), oversample: float = None):
        """
        data is (image_fnames, gts)
        :param data:
        :param batch_size:
        :param num_threads_in_multithreaded:
        :param seed_for_shuffle:
        :param return_incomplete:
        :param shuffle:
        :param infinite:
        """
        super().__init__(data, batch_size, num_threads_in_multithreaded, seed_for_shuffle, return_incomplete,
                         shuffle, infinite)
        self.image_size = image_size
        self.indices = list(range(len(data[0])))

        if oversample is None:
            self.p = None
        else:
            num_single = np.sum(data[1])
            num_samples = len(data[1])
            num_others = num_samples - num_single
            p_single = oversample / num_single
            p_others = (1 - oversample) / num_others
            p = [p_single if data[1][i] == 1 else p_others for i in range(num_samples)]
            self.p = np.array(p) / sum(p)
            print(np.sum(self.p[np.array(data[1]) == 1]), np.sum(self.p[np.array(data[1]) == 0]))

    def generate_train_batch(self):
        idx = self.get_indices()
        data = np.zeros((len(idx), 1, *self.image_size), dtype=np.float32)
        target = np.zeros(len(idx), dtype=np.int)
        fnames = []
        for j, i in enumerate(idx):
            data[j, 0] = np.load(self._data[0][i])['data']
            target[j] = self._data[1][i]
            fnames.append(self._data[0][i])
        return {'data': data, 'target': target, 'fnames': fnames}

    def get_indices(self):
        # add self.p usage in np.random.choice
        if self.infinite:
            return np.random.choice(self.indices, self.batch_size, replace=True, p=self.p)

        if self.last_reached:
            self.reset()
            raise StopIteration

        if not self.was_initialized:
            self.reset()

        indices = []

        for b in range(self.batch_size):
            if self.current_position < len(self.indices):
                indices.append(self.indices[self.current_position])

                self.current_position += 1
            else:
                self.last_reached = True
                break

        if len(indices) > 0 and ((not self.last_reached) or self.return_incomplete):
            self.current_position += (self.number_of_threads_in_multithreaded - 1) * self.batch_size
            return indices
        else:
            self.reset()
            raise StopIteration


if __name__ == '__main__':
    train_files, train_gt, val_files, val_gt, test_files, test_gt = load_5fold_newDataSplit_v2()
    dl = SPIDataLoader((train_files, train_gt), 128, 1, None, True, True, False)
    sizes = []
    for b in dl:
        sizes.append(b['data'].shape)
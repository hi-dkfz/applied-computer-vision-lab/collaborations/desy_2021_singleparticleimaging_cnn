from multiprocessing import Pool
from typing import List

import matplotlib.pyplot as plt
import numpy as np

from spi_classification.paths import image_source_folder


def log_scale(img, scale_to_utin8=False):
    img = img.astype(np.float64)
    img[img != 0] = (np.log(img[img != 0]) + 1)
    if scale_to_utin8:
        img = scale_img_to_uint8(img)
    return img


def scale_img_to_uint8(img):
    img = img.astype(np.float)
    img = img / max(1e-8, img.max()) * 255
    img = np.round(img).astype(np.uint8)
    return img


def export_png(arr, fname, prediction, gt=None, softmax_prob=None, fold=None, cmap='gray', transform=None, text_color=[1, 1, 1]):
    if transform is not None:
        arr = transform(np.copy(arr))

    plt.figure()
    plt.imshow(scale_img_to_uint8(arr), cmap=cmap)

    if gt is not None:
        plt.text(0, 10, "Ground truth: %d" % gt, color=text_color)
    if prediction is not None:
        plt.text(0, 20, "Prediction:      %d" % prediction, color=text_color)
    if softmax_prob is not None:
        plt.text(0, 30, "Probability:     %0.4f" % softmax_prob, color=text_color)
    if fold is not None:
        plt.text(0, 40, "Fold%s" % str(fold), color=text_color)

    plt.savefig(fname)
    plt.close()


def export_png_from_npz(npz_file, fname, prediction=None, gt=None, softmax_prob=None, fold=None, cmap='gray', transform=None, text_color=[1, 1, 1]):
    return export_png(np.load(npz_file)['data'], fname, prediction, gt, softmax_prob, fold, cmap, transform, text_color)


def export_npz_to_png(npz_files: List[str], output_files: List[str], num_processes: int = 8, prediction=None, gt=None,
                      softmax_prob=None, fold=None, cmap='gray',
                      transform=None, text_color=[1, 1, 1]):
    p = Pool(num_processes)
    num = len(npz_files)
    r = p.starmap(export_png_from_npz, zip(npz_files, output_files, [prediction] * num, [gt] * num,
                                  [softmax_prob] * num, [fold] * num, [cmap] * num, [transform] * num, [text_color] * num))
    p.close()
    p.join()


if __name__ == '__main__':
    from batchgenerators.utilities.file_and_folder_operations import *
    csv_file = '/home/fabian/Downloads/over005_moreSingleHits_w_005_095.csv'
    data = np.loadtxt(csv_file, dtype=str, delimiter=',', skiprows=1)
    predicted_single_hits = data[:, 0][data[:, 2] == '1']
    gt_single_hits = data[:, 0][data[:, 3] == '1']

    output_folder_pred = '/home/fabian/temp/single_hits/pred'
    output_folder_gt = '/home/fabian/temp/single_hits/gt'

    output_files_pred = [join(output_folder_pred, i[:-4] + '.png') for i in predicted_single_hits]
    output_files_gt = [join(output_folder_gt, i[:-4] + '.png') for i in gt_single_hits]

    predicted_single_hits = [join(image_source_folder, i) for i in predicted_single_hits]
    gt_single_hits = [join(image_source_folder, i) for i in gt_single_hits]

    maybe_mkdir_p(output_folder_pred)
    maybe_mkdir_p(output_folder_gt)

    #export_npz_to_png(predicted_single_hits, output_files_pred, cmap='jet')
    #export_npz_to_png(gt_single_hits, output_files_gt, cmap='jet')

    csv_file = '/media/fabian/data/DESY/training_validation/summary_18k_55-84nm_EMGT.csv'
    data = np.loadtxt(csv_file, dtype=str, delimiter=',', skiprows=1)
    emgt_single_hits = data[:, 0][data[:, -1] == '1.0']

    output_folder_emgt = '/home/fabian/temp/single_hits/emgt'

    output_files_emgt = [join(output_folder_emgt, i + '.png') for i in emgt_single_hits]

    emgt_single_hits = [join(image_source_folder, i + '.npz') for i in emgt_single_hits]

    maybe_mkdir_p(output_folder_emgt)

    export_npz_to_png(emgt_single_hits, output_files_emgt, cmap='jet')


import numpy as np
from batchgenerators.utilities.file_and_folder_operations import isfile


def crop_image(data: np.ndarray, patch_size=(192, 96)):
    """
    :param data:
    :param patch_size:
    :return:
    """
    data_shape = data.shape
    data = data[data_shape[0] // 2 - patch_size[0] // 2:data_shape[0] // 2 + patch_size[0] // 2,
           data_shape[1] - patch_size[1]:]
    return data


def crop_and_save(image, output_filename, patch_size=(192, 96)):
    image = crop_image(image, patch_size)
    if isfile(output_filename):
        raise IOError()
    np.savez_compressed(output_filename, data=image)

# Introduction
<img src="SPI_pattern_classification.png" width="1080px" />

In single particle imaging with X-ray-free electron lasers (XFEL), biological particles such as viruses or 
protein complexes are injected in the intense femtosecond XFEL beam in their native environment and 
diffraction patterns are collected. These patterns, if numerous enough, can then be used for the 3D 
reconstruction of the particle of interest.

As part of the reconstruction pipeline, patterns originating from so called single hits (i.e. one 
particle of interest being hit by the laser) must be isolated from non-single hits (empty patterns, 
patterns originating from multiple particles, partial hits, patterns from surrounding media, ...). 

Here we formulate the single hit identification problem as a canonical image classification task and train a CNN to 
perform the distinction. Importantly, the CNN is applied to the dataset prior to particle size selection and 
traditional EM-based algorithms, allowing its application on the fly during pattern acquisition, thus having the 
potential to a) greatly reduce the amount of data that needs to be saved during an experiment and b) terminate the 
experiment once sufficient single hits have been acquired (as opposed to guessing). 

The dataset used in this experiment originates from the amo34117 experiment. It was published on the 
[CXDI platform](https://www.cxidb.org/id-156.html). We use the single hit selection published in (Li et al., 2020) as 
a ground truth for training and testing. The data was preprocessed (background correction, center estimation) using the 
procedure outlined in (Bobkov et al., 2020, coda available [here](https://gitlab.com/spi_xfel)). 
For your convenience, the preprocessed dataset is provided [here](https://zenodo.org/record/6451444).


For more information, please take a look at our publication: 
```
NOT YET PUBLISHED
```
We encourage you to cite it in your work!

# Installation instructions

1) We recommend you create a fresh new pip or conda environment for this project! 
   For example: `conda create --name spi_cnn python=3.9 anaconda`. Activate it with `conda activate spi_cnn`.
2) Install pytorch by following the instructions on the [PyTorch Website](https://pytorch.org/get-started/locally/). 
   Pick the most recent CUDA version that is supported by your system.
3) We build our training procedure on nnU-Net. Install it like this: 
   ```
   pip install git+https://github.com/MIC-DKFZ/nnUNet#c0a999f80aac464f115ed3cff45d22b22d47c8fb
   ```
   Note: if you already have installed nnU-Net you have to uninstall the old version first (`pip uninstall nnunet -y`)! 
   Otherwise it won't work. (again: please start with a fresh new virtual environment, see step 1!)
   (note that we specifically set a certain commit hash to ensure compatibility. Do not remove that!)
4) Data augmentation is handled with batchgenerators. Install it like this:
   ```
   pip install git+https://github.com/MIC-DKFZ/batchgenerators#ea0189973fbb6f0b2dbf7018d3c3b1cdab5dfc5f
   ```
   Also here, if you have batchgenerators already installed, uninstall it first. Best to start with a fresh environment ;-)
5) Clone this repository and install it by executing `pip install -e .` in the folder where the setup.py file is located


# Usage instructions
1) Download and extract the preprocessed data from this [link](https://zenodo.org/record/6451444) (file: `data_corrected.zip`).
2) Open `spi_classification/paths.py` and modify the line `base = os.environ.get('DESY_BASE')` so that 
   it points to a folder on your system. This is where the patches for training/testing as well as the
   results will be saved (Example: `base = "/home/fabian/SPI/"`)
3) Convert preprocessed datasets into patches. These are used for cross-validation (model development) and final testing:
   ```bash
   python spi_classification/data_preparation_and_management/200k_dataset.py INPUT_FOLDER -num_processes 8
   ```
   Where `INPUT_FOLDER` is the extracted data (the folder where the .h5 files are located). Use as many CPU cores 
   as you like (`-num_processes`)
4) Copy the `final_200k_split_191183.pkl` file from our [zenodo repository](https://zenodo.org/record/6451444) 
into the folder specified in step 2.
5) Now you can run the training: 
    ```bash
    python spi_classification/run_training_5fold.py PaperConfigMaxF1 0
    python spi_classification/run_training_5fold.py PaperConfigMaxF1 1
    python spi_classification/run_training_5fold.py PaperConfigMaxF1 2
    python spi_classification/run_training_5fold.py PaperConfigMaxF1 3
    python spi_classification/run_training_5fold.py PaperConfigMaxF1 4
    ```
   Alternatively you can also just download our pretrained weights (see below).
6) If you'd like to summarize your cross-validation run, \
   `python spi_classification/summarize_crossvalidation_results.py`\
   This will create a `summary_cv.csv` in your results folder. Remember that this shows the average results 
between the 5 folds (average FP, FN, etc).
7) To predict the test set, run \
   `python spi_classification/predict_test_set.py PaperConfigMaxF1 -f 0 1 2 3 4 --tta`\
   You can optionally specify an output file with `-o OUTPUT_FILE` where the metrics and predictions will be saved. 
   Note that this file will automatically be extended with .json and .csv for metrics and predictions, respectively.

**All the scripts mentioned in these instructions supper the `-h` flag! Use it so see additional options.**

## Pretrained weights
Pretrained model weights are publicly available! Download them [here](https://zenodo.org/record/6451444) 
(file: `PaperConfigMaxF1_final.zip`), extract them in `base/results` (where base is 
the folder specified in 2. of the installation instructions). The structure should look like this:
`base/results/PaperConfigMaxF1/fold_x` (where fold_x goes from 0 to 4).

### References
Assalauova, D., Ignatenko, A., Isensee, F., et al. “Classification of diffraction patterns using a convolutional 
neural network in single particle imaging experiments performed at X-ray free-electron lasers.” arXiv preprint 
arXiv:2112.09020.

Li, Haoyuan, et al. ”Diffraction data from aerosolized Coliphage PR772 virus particles imaged with the Linac Coherent 
Light Source.” Scientific data 7.1 (2020): 1-12.

Bobkov, S. A., et al. “Advances in Modern Information Technologies for Data Analysis in CRYO-EM and XFEL Experiments.” 
Crystallography Reports 65.6 (2020): 1081-1092.

### Acknowledgement
<img src="HI_Logo.png" width="512px" />

Part of this work was funded by Helmholtz Imaging (HI), a platform of the Helmholtz Incubator on Information and Data Science.

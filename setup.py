from setuptools import setup, find_namespace_packages

setup(name='spi_classification',
      packages=find_namespace_packages(include=["spi_classification", "spi_classification.*"]),
      version='0.0',
      description='Code for publication \'Classification of diffraction patterns using a convolutional neural '
                  'network in single particle imaging experiments performed at X-ray free-electron lasers\'',
      author='Fabian Isensee, Helmholtz Imaging Applied Domputer Vision Lab',
      author_email='f.isensee@dkfz-heidelberg.de',
      install_requires=[
            "torch>=1.6.0a",
            "batchgenerators>=0.23",
            "nnunet",
            "numpy",
            "h5py",
      ],
      )
